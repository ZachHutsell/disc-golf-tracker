const functions = require('firebase-functions');
const express = require('express');
const app = express();

app.use(express.json());// to support JSON-encoded bodies

const repo = require('./repo.js');

app.get('/timestamp', function (req, res) {
    res.send(`${Date.now()}`);
});

app.get('/users/:userId/rounds', function(req, res) {
    repo.getPlayerRounds(req.params.userId, function(data) {
        res.json(data);
    });
});

app.get('/rounds', function (req, res) {
    repo.getRounds(req, res);
});
app.post('/rounds', function (req, res) {
    repo.addRound(req, res);
});

exports.app = functions.https.onRequest(app);