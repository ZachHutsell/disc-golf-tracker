const admin = require('firebase-admin');
const functions = require('firebase-functions');

admin.initializeApp(functions.config().firebase);

var firestore = admin.firestore();

exports.getPlayerRounds = function (userId, cb) {
    var arr = [];
    firestore.collection('rounds')
        .where('userId', '==', userId)
        .get()
        .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                arr.push(doc.data());
            });
            cb(arr);
        });
}

//todo need to refactor methods to not handle req, res
exports.getRounds = function (req, res) {
    var arr = [];
    firestore.collection('rounds').get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            const data = JSON.parse(JSON.stringify(doc.data()));
            arr.push(data);
        });

        res.send(arr);
    });
}

exports.addRound = function (req, res) {
    firestore.collection('rounds')
        .add(req.body)
        .then(function (docRef) {
            res.send(docRef);
        })
        .catch(function (error) {
            res.send(error);
        });
}