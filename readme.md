https://disc-golf-tracker-72c7e.firebaseapp.com/

Recommend using `npm run firebase-serve` script for local development

able to serve and deploy on node v8.11.2. `nvm use --lts=carbon`

.gitignore below
-----------------
.gitignore
node_modules
firebase-debug.log
public/dist
