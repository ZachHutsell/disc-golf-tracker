const repo = require('./repo');
const playerCard = require('./playerCard');

Vue.component('hole', {
	template:
		'<div>' +
		'<h1>{{round.location}}</h1>' +
		'<h2>Hole {{number}}</h2>' +
		'<div>' +
		'<div class="form-group row">' +
		'<div class="col-2">Par</div>' +
		'<div class="form-check-inline">' +
		'<input class="form-check-input" type="radio" name="par" value="3" v-model="hole.par">' +
		'<label class="form-check-label">3</label>' +
		'</div>' +
		'<div class="form-check-inline">' +
		'<input class="form-check-input" type="radio" name="par" value="4" v-model="hole.par">' +
		'<label class="form-check-label">4</label>' +
		'</div>' +
		'<div class="form-check-inline">' +
		'<input class="form-check-input" type="radio" name="par" value="5" v-model="hole.par">' +
		'<label class="form-check-label">5</label>' +
		'</div>' +
		'</div>' +
		'<div class="form-group card-deck">' +
		'<playerCard v-for="player in round.players" @stroke-count-changed="onStrokeCountChanged" v-bind:key="player.id"  v-bind:isEmptyCard="true" v-bind:id="player.id" v-bind:name="player.name"></playerCard>' +
		'</div>' +
		'<div class="row">' +
		'<div class="col-7" v-if="notFirstHole"><button class="btn btn-secondary" v-on:click="goToPreviousHole">Previous Hole</button></div>' +
		'<div class="col"><button class="btn btn-primary right-block" v-on:click="goToNextHole">Next Hole</button></div>' +
		'</div>' +
		'<div class="row">' +
		'<div class="col offset-3"><button class="btn btn-light" v-on:click="showScoreboard">Show scoreboard</button></div>' +
		'</div>' +
		'</div>' +
		'</div>',
	props: ['number'],
	data: function () {
		var round = repo.getRound();
		var hole = repo.getRound().getHoleByNumber(this.number);

		return {
			round: round,
			hole: hole,
		}
	},
	computed: {
		notFirstHole: function () {
			return this.number > 1
		}
	},
	methods: {
		onStrokeCountChanged: function (id, strokes) {
			this.hole.setPlayerScoreModel(id, strokes);
		},
		goToPreviousHole: function () {
			this.$emit('go-to-hole', this.number - 1);
		},
		goToNextHole: function () {
			this.hole.completed = true;
			this.$emit('go-to-hole', this.number + 1);
		},
		showScoreboard: function () {
			this.$emit('show-scoreboard');
		}
	}
});