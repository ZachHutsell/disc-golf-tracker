Vue.component('playerCard', {
    template:
        '<div class="card">' +
        '<div class="card-header">{{name}}</div>' +
        '<div v-if="isEmptyCard" class="card-body">' +
        '<div class="row">' +
        '<div class="col"><button v-on:click="subtractStroke" class="float-right btn btn-lg btn-secondary">-1</button></div>' +
        '<h3>{{strokes}}</h3>' +
        '<div class="col"><button v-on:click="addStroke" type="button" class="btn btn-lg btn-primary">+1</button></div>' +
        '</div>' +
        '</div>' +
        '</div>',
    props: ['id', 'name', 'isEmptyCard'],
    data: function () {
        return {
            strokes: 0
        };
    },
    methods: {
        addStroke: function(event) {
            this.strokes++;
            event.preventDefault();
        },
        subtractStroke: function(event) {
            this.strokes--;
            event.preventDefault();
        }
    },
    watch: {
        strokes: function () {
            this.$emit('stroke-count-changed', this.id, parseInt(this.strokes));
        }
    }
});
