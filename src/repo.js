import Round from './round';
const axios = require('axios');

var round = {};

function setup(rawRound) {
	round = new Round(rawRound.location, rawRound.holeCount, rawRound.players);
}

function getRound() {
	return round;
}

function postRound(userId) {
	round.userId = userId;
	axios.post('/rounds',
		JSON.parse(JSON.stringify(round)))
		.then(function (res) {
			console.log(JSON.stringify(res.data));
		})
		.catch(function (error) {
			console.log(error);
		});
}

export { setup, getRound, postRound}