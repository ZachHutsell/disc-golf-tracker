const _ = require('underscore');
const defaultPar = 3;

class Hole {
	constructor(number, par, scores) {
		this.number = number;
		this.par = par;
		this.scores = scores;
		this.completed = false;
	}

	getPlayerScore(playerId) {
		var model = _.findWhere(this.scores, { playerId: playerId });
		return model.strokes;
	}

	setPlayerScoreModel(playerId, strokes) {
		var model = _.findWhere(this.scores, { playerId: playerId });
		model.strokes = strokes;
	}
}

export default class Round {
	constructor(location, holeCount, players) {
		this.location = location;
		this.holes = this.initHoles(holeCount, players);
		this.players = players;
	}

	initHoles(holeCount, players) {
		var holes = [];
		for (var i = 1; i <= holeCount; i++) {
			holes.push(new Hole(i, defaultPar, this.initScoresForHole(players)));
		}
		return holes;
	}

	initScoresForHole(players) {
		var scoreModels = [];
		_.each(players, function (player) {
			scoreModels.push({
				playerId: player.id,
				strokes: 0
			});
		});
		return scoreModels;
	}

	getCompletedHoles() {
		return _.where(this.holes,  {completed: true});
	}

	getRoundPar() {
		return _.reduce(this.getCompletedHoles(), function (memo, hole) {
			return memo + parseInt(hole.par);
		}, 0);
	}

	getRoundScoreForPlayer(playerId) {
		var score = 0;
		_.each(this.getCompletedHoles(), function (hole) {
			score = score + hole.getPlayerScore(playerId);
		});

		return score;
	}

	getHoleByNumber(holeNumber) {
		return _.findWhere(this.holes, { number: holeNumber });
	}

	logRound() {
		console.log(JSON.parse(JSON.stringify(this)));
	}
}
