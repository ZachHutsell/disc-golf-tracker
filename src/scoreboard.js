const _ = require('underscore');
const repo = require('./repo');

Vue.component('scoreboard', {
	template:
		'<div>' +
		'<h1>{{round.location}}</h1>' +
		'<h2>Scoreboard</h2>' +
		'<table class="table">' +
		'<thead>' +
		'<tr>' +
		'<th scope="col">Hole</th>' +
		'<th scope="col">Par</th>' +
		'<th scope="col" v-for="player in round.players">{{player.name}}</th>' +
		'</tr>' +
		'</thead>' +
		'<tbody>' +
		'<tableRow v-for="hole in round.holes" v-if="hole.completed" v-bind:key="hole.number" v-bind:number="hole.number" v-bind:par="hole.par" v-bind:scores="getHoleScores(hole.number)"></tableRow>' +
		'<tableRow v-bind:number="total" v-bind:par="round.getRoundPar()" v-bind:scores="getRoundScores()"></tableRow>' +
		'</tbody>' +
		'</table>' +
		'<div v-if="showHideScoreboardButton" class="row">' +
		'<div class="col offset-3"><button class="btn btn-light" v-on:click="hideScoreboard">Hide scoreboard</button></div>' +
		'</div>' +
		'<div v-if="!showHideScoreboardButton" class="pt-3 row">' +
		'<div class="col"><input type="text" v-model="userId" placeHolder="User ID" /></div>' +
		'<div class="col"><button class="btn btn-primary" v-bind:disabled="!userId" v-on:click="postRound">Save round</button></div>' +
		'</div>' +
		'</div>',
	props: ['done'],
	data: function () {
		var round = repo.getRound();
		return {
			round: round,
			total: 'Total score',
			userId: null
		}
	},
	computed: {
		showHideScoreboardButton: function () {
			return !this.done;
		}
	},
	methods: {
		getHoleScores: function (holeNumber) {
			var scores = [];
			var hole = this.round.getHoleByNumber(holeNumber);
			_.each(this.round.players, function (player) {
				scores.push(hole.getPlayerScore(player.id));
			});
			return scores;
		},
		getRoundScores: function () {
			var scores = [];
			_.each(this.round.players, function (player) {
				scores.push(this.round.getRoundScoreForPlayer(player.id));
			}, this);
			return scores;
		},
		hideScoreboard: function () {
			this.$emit('hide-scoreboard');
		},
		postRound: function() {
			repo.postRound(this.userId);
		}
	}
});

Vue.component('tableRow', {
	template:
		'<tr>' +
		'<th scope="row">{{number}}</th>' +
		'<td>{{par}}</td>' +
		'<td v-for="score in scores" v-bind:class="getClass(score)">{{score}}</td>' +
		'</tr>',
	props: ['number', 'par', 'scores'],
	methods: {
		getClass: function (score) {
			if (score < this.par) {
				return 'my-text-success';
			} else if (score > this.par) {
				return 'my-text-danger';
			}
		}
	}
});
