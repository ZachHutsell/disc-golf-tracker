const repo = require('./repo.js');
const playerCard = require('./playerCard');

const template =
	'<div>' +
	'<h1>New round setup</h1>' +
	'<div>' +
	'<div class="form-group">' +
	'<input id="location" v-model="location" type="text" class="form-control" placeHolder="Course name"/>' +
	'</div>' +
	'<div class="form-group">' +
	'<input id="holeCount" v-model="holeCount" type="tel" class="form-control" placeHolder="Hole count"/>' +
	'</div>' +
	'<div class="form-group">' +
	'<input type="text" @keyup.enter="addPlayer" v-model="nameInProgress"class="form-control" placeholder="Player name">' +
	'<small class="form-text my-text-muted">Press enter to add player card</small>' +
	'</div>' +
	'<div class="form-group card-deck">' +
	'<playerCard v-for="player in players" v-bind:key="player.id" v-bind:id="player.id" v-bind:isEmptyCard="false" v-bind:name="player.name"></playerCard>' +
	'</div>' +
	'<button v-on:click="completeSetup()" onClick="return false" class="btn btn-primary right-block">Start round</button>' +
	'</div>' +
	'</div>';

const playerItemTemplate =
	'<li class="list-group-item">{{name}}</li>'

Vue.component('setup', {
	template: template,
	data: function () {
		return {
			location: null,
			holeCount: null,
			nameInProgress: null,
			nextPlayerId: 1,
			players: []
		};
	},
	methods: {
		completeSetup: function () {
			if (this.nameInProgress) {
				this.addPlayer();
			}
			var roundData = {
				location: this.location,
				holeCount: this.holeCount,
				players: this.players
			}
			repo.setup(roundData);
			this.$emit('setup-complete');
		},
		addPlayer: function () {
			if (this.nameInProgress)
				this.players.push({ id: this.nextPlayerId, name: this.nameInProgress });
			this.nextPlayerId++;
			this.nameInProgress = '';
		}
	}
});

Vue.component('playerItem', {
	template: playerItemTemplate,
	props: ['id', 'name']
});
