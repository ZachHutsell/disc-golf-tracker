const setup = require('./setup.js');
const hole = require('./hole.js');
const scoreboard = require('./scoreboard.js');
const repo = require('./repo.js')

new Vue({
	el: '#app',
	data: function () {
		return {
			setupComplete: false,
			currentHole: 1,
			scoreboardVisible: false,
			roundComplete: false,
			round: {}
		}
	},
	methods: {
		completeSetup: function () {
			this.setupComplete = true;
			this.round = repo.getRound();
		},
		goToHole: function (holeNumber) {
			this.currentHole = holeNumber;
			if (this.currentHole > this.round.holes.length) {
				this.roundComplete = true;
				this.showScoreboard();
			}
		},
		showScoreboard: function () {
			this.scoreboardVisible = true;
		},
		hideScoreboard: function () {
			this.scoreboardVisible = false;
		},
		holeInProgress: function (holeNumber) {
			return this.setupComplete && !this.scoreboardVisible && this.currentHole == holeNumber
		}
	}
});
